Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip]
"BootFlags"=dword:00000001
"Description"="@%SystemRoot%\\system32\\tcpipcfg.dll,-50003"
"DisplayName"="@%SystemRoot%\\system32\\drivers\\tcpip.sys,-10001"
"ErrorControl"=dword:00000001
"Group"="PNP_TDI"
"ImagePath"=hex(2):53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,64,00,\
  72,00,69,00,76,00,65,00,72,00,73,00,5c,00,74,00,63,00,70,00,69,00,70,00,2e,\
  00,73,00,79,00,73,00,00,00
"Start"=dword:00000000
"Tag"=dword:00000003
"Type"=dword:00000001
"NdisMajorVersion"=dword:00000006
"NdisMinorVersion"=dword:00000028
"DriverMajorVersion"=dword:00000000
"DriverMinorVersion"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Linkage]
"Export"=hex(7):5c,00,44,00,65,00,76,00,69,00,63,00,65,00,5c,00,54,00,63,00,70,\
  00,69,00,70,00,5f,00,7b,00,44,00,30,00,35,00,34,00,30,00,46,00,34,00,45,00,\
  2d,00,44,00,42,00,31,00,46,00,2d,00,34,00,36,00,35,00,37,00,2d,00,41,00,32,\
  00,30,00,45,00,2d,00,34,00,35,00,31,00,30,00,39,00,33,00,44,00,35,00,35,00,\
  38,00,44,00,45,00,7d,00,00,00,5c,00,44,00,65,00,76,00,69,00,63,00,65,00,5c,\
  00,54,00,63,00,70,00,69,00,70,00,5f,00,7b,00,45,00,45,00,46,00,42,00,42,00,\
  44,00,32,00,36,00,2d,00,37,00,38,00,31,00,35,00,2d,00,34,00,44,00,44,00,32,\
  00,2d,00,39,00,46,00,41,00,43,00,2d,00,30,00,42,00,46,00,37,00,37,00,43,00,\
  43,00,44,00,43,00,44,00,46,00,39,00,7d,00,00,00,00,00
"Bind"=hex(7):5c,00,44,00,65,00,76,00,69,00,63,00,65,00,5c,00,7b,00,44,00,30,\
  00,35,00,34,00,30,00,46,00,34,00,45,00,2d,00,44,00,42,00,31,00,46,00,2d,00,\
  34,00,36,00,35,00,37,00,2d,00,41,00,32,00,30,00,45,00,2d,00,34,00,35,00,31,\
  00,30,00,39,00,33,00,44,00,35,00,35,00,38,00,44,00,45,00,7d,00,00,00,5c,00,\
  44,00,65,00,76,00,69,00,63,00,65,00,5c,00,7b,00,45,00,45,00,46,00,42,00,42,\
  00,44,00,32,00,36,00,2d,00,37,00,38,00,31,00,35,00,2d,00,34,00,44,00,44,00,\
  32,00,2d,00,39,00,46,00,41,00,43,00,2d,00,30,00,42,00,46,00,37,00,37,00,43,\
  00,43,00,44,00,43,00,44,00,46,00,39,00,7d,00,00,00,00,00
"Route"=hex(7):22,00,7b,00,44,00,30,00,35,00,34,00,30,00,46,00,34,00,45,00,2d,\
  00,44,00,42,00,31,00,46,00,2d,00,34,00,36,00,35,00,37,00,2d,00,41,00,32,00,\
  30,00,45,00,2d,00,34,00,35,00,31,00,30,00,39,00,33,00,44,00,35,00,35,00,38,\
  00,44,00,45,00,7d,00,22,00,00,00,22,00,7b,00,45,00,45,00,46,00,42,00,42,00,\
  44,00,32,00,36,00,2d,00,37,00,38,00,31,00,35,00,2d,00,34,00,44,00,44,00,32,\
  00,2d,00,39,00,46,00,41,00,43,00,2d,00,30,00,42,00,46,00,37,00,37,00,43,00,\
  43,00,44,00,43,00,44,00,46,00,39,00,7d,00,22,00,00,00,00,00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters]
"DataBasePath"=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,6f,\
  00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,\
  64,00,72,00,69,00,76,00,65,00,72,00,73,00,5c,00,65,00,74,00,63,00,00,00
"Domain"="adaudit77.lt"
"ForwardBroadcasts"=dword:00000000
"ICSDomain"="mshome.net"
"IPEnableRouter"=dword:00000000
"NameServer"=""
"SyncDomainWithMembership"=dword:00000001
"NV Hostname"="APPAD1"
"Hostname"="APPAD1"
"DhcpDomain"="reddog.microsoft.com"
"DhcpNameServer"="10.1.0.10 10.1.0.11"
"NV Domain"="adaudit77.lt"
"ShutDownTimeAtLastDomainJoin"=hex:5b,9a,17,ec,71,e3,d7,01

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Adapters]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Adapters\{d0540f4e-db1f-4657-a20e-451093d558de}]
"LLInterface"=""
"IpConfig"=hex(7):54,00,63,00,70,00,69,00,70,00,5c,00,50,00,61,00,72,00,61,00,\
  6d,00,65,00,74,00,65,00,72,00,73,00,5c,00,49,00,6e,00,74,00,65,00,72,00,66,\
  00,61,00,63,00,65,00,73,00,5c,00,7b,00,44,00,30,00,35,00,34,00,30,00,46,00,\
  34,00,45,00,2d,00,44,00,42,00,31,00,46,00,2d,00,34,00,36,00,35,00,37,00,2d,\
  00,41,00,32,00,30,00,45,00,2d,00,34,00,35,00,31,00,30,00,39,00,33,00,44,00,\
  35,00,35,00,38,00,44,00,45,00,7d,00,00,00,00,00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Adapters\{eefbbd26-7815-4dd2-9fac-0bf77ccdcdf9}]
"LLInterface"=""
"IpConfig"=hex(7):54,00,63,00,70,00,69,00,70,00,5c,00,50,00,61,00,72,00,61,00,\
  6d,00,65,00,74,00,65,00,72,00,73,00,5c,00,49,00,6e,00,74,00,65,00,72,00,66,\
  00,61,00,63,00,65,00,73,00,5c,00,7b,00,45,00,45,00,46,00,42,00,42,00,44,00,\
  32,00,36,00,2d,00,37,00,38,00,31,00,35,00,2d,00,34,00,44,00,44,00,32,00,2d,\
  00,39,00,46,00,41,00,43,00,2d,00,30,00,42,00,46,00,37,00,37,00,43,00,43,00,\
  44,00,43,00,44,00,46,00,39,00,7d,00,00,00,00,00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\DNSRegisteredAdapters]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\DNSRegisteredAdapters\{EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}]
"Hostname"="APPAD1"
"AdapterDomainName"=""
"PrimaryDomainName"="adaudit77.lt"
"HostAddrs"=hex:01,00,00,00,01,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,02,00,00,00,0a,01,01,04,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,10,00,00,00,18,00,00,\
  00,01,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
"DnsServers"=hex:02,00,00,00,02,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,02,00,00,00,0a,01,00,0a,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,10,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,02,00,00,00,0a,01,00,0b,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,10,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
"SentUpdateToIp"=hex:00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
"SentPriUpdateToIp"=hex:02,00,00,35,0a,01,00,0a,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,10,00,00,00,00,00,00,00,00,00,00,\
  00,ff,ff,ff,ff,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
"Ttl"=dword:000004b0
"Flags"=dword:00000002
"CompartmentId"=dword:00000001
"StaleAdapter"=dword:00000000
"RegisteredSinceBoot"=dword:00000001

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces\{9af00f37-3d0b-11ec-a6d7-806e6f6e6963}]
"EnableDhcp"=dword:00000001

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces\{d0540f4e-db1f-4657-a20e-451093d558de}]
"EnableDHCP"=dword:00000001
"Domain"=""
"NameServer"=""

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces\{eefbbd26-7815-4dd2-9fac-0bf77ccdcdf9}]
"EnableDHCP"=dword:00000001
"Domain"=""
"NameServer"=""
"DhcpIPAddress"="10.1.1.4"
"DhcpSubnetMask"="255.255.255.0"
"DhcpServer"="168.63.129.16"
"Lease"=dword:ffffffff
"LeaseObtainedTime"=dword:6267f9f9
"T1"=dword:e267f9f6
"T2"=dword:e267f9f6
"LeaseTerminatesTime"=dword:e267f9f6
"AddressType"=dword:00000000
"IsServerNapAware"=dword:00000000
"DhcpConnForceBroadcastFlag"=dword:00000000
"DhcpDomain"="reddog.microsoft.com"
"DhcpNameServer"="10.1.0.10 10.1.0.11"
"DhcpDefaultGateway"=hex(7):31,00,30,00,2e,00,31,00,2e,00,31,00,2e,00,31,00,00,\
  00,00,00
"DhcpSubnetMaskOpt"=hex(7):32,00,35,00,35,00,2e,00,32,00,35,00,35,00,2e,00,32,\
  00,35,00,35,00,2e,00,30,00,00,00,00,00
"DhcpInterfaceOptions"=hex:fc,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,\
  51,01,00,79,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,51,01,00,77,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,51,01,00,2f,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,82,51,01,00,2e,00,00,00,00,00,00,00,00,00,00,00,\
  00,00,00,00,82,51,01,00,2c,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,\
  51,01,00,2b,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,51,01,00,21,00,\
  00,00,00,00,00,00,00,00,00,00,00,00,00,00,82,51,01,00,1f,00,00,00,00,00,00,\
  00,00,00,00,00,00,00,00,00,82,51,01,00,0f,00,00,00,00,00,00,00,14,00,00,00,\
  00,00,00,00,01,00,00,00,72,65,64,64,6f,67,2e,6d,69,63,72,6f,73,6f,66,74,2e,\
  63,6f,6d,3b,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,ff,ff,\
  ff,ff,3a,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,ff,ff,ff,\
  ff,33,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,ff,ff,ff,ff,\
  f5,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,a8,3f,81,10,06,\
  00,00,00,00,00,00,00,08,00,00,00,00,00,00,00,01,00,00,00,0a,01,00,0a,0a,01,\
  00,0b,03,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,0a,01,01,\
  01,01,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,ff,ff,ff,00,\
  36,00,00,00,00,00,00,00,04,00,00,00,00,00,00,00,01,00,00,00,a8,3f,81,10,35,\
  00,00,00,00,00,00,00,01,00,00,00,00,00,00,00,01,00,00,00,05,00,00,00
"DhcpGatewayHardware"=hex:0a,01,01,01,06,00,00,00,12,34,56,78,9a,bc
"DhcpGatewayHardwareCount"=dword:00000001

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\NsiObjectSecurity]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\PersistentRoutes]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Winsock]
"UseDelayedAcceptance"=dword:00000000
"MaxSockAddrLength"=dword:00000010
"MinSockAddrLength"=dword:00000010
"HelperDllName"=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,\
  6f,00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,\
  00,77,00,73,00,68,00,74,00,63,00,70,00,69,00,70,00,2e,00,64,00,6c,00,6c,00,\
  00,00
"ProviderGUID"=hex:a0,1a,0f,e7,8b,ab,cf,11,8c,a3,00,80,5f,48,a1,92
"OfflineCapable"=dword:00000001
"Mapping"=hex:08,00,00,00,03,00,00,00,02,00,00,00,01,00,00,00,06,00,00,00,02,\
  00,00,00,01,00,00,00,00,00,00,00,02,00,00,00,00,00,00,00,06,00,00,00,02,00,\
  00,00,02,00,00,00,11,00,00,00,02,00,00,00,02,00,00,00,00,00,00,00,02,00,00,\
  00,00,00,00,00,11,00,00,00,02,00,00,00,03,00,00,00,ff,00,00,00,02,00,00,00,\
  03,00,00,00,00,00,00,00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Winsock\0]
"Version"=dword:00000002
"AddressFamily"=dword:00000002
"MaxSockAddrLength"=dword:00000010
"MinSockAddrLength"=dword:00000010
"SocketType"=dword:00000001
"Protocol"=dword:00000006
"ProtocolMaxOffset"=dword:00000000
"ByteOrder"=dword:00000000
"MessageSize"=dword:00000000
"szProtocol"=hex(2):40,00,25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,\
  00,6f,00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,\
  5c,00,6d,00,73,00,77,00,73,00,6f,00,63,00,6b,00,2e,00,64,00,6c,00,6c,00,2c,\
  00,2d,00,36,00,30,00,31,00,30,00,30,00
"ProviderFlags"=dword:00000008
"ServiceFlags"=dword:00020066

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Winsock\1]
"Version"=dword:00000002
"AddressFamily"=dword:00000002
"MaxSockAddrLength"=dword:00000010
"MinSockAddrLength"=dword:00000010
"SocketType"=dword:00000002
"Protocol"=dword:00000011
"ProtocolMaxOffset"=dword:00000000
"ByteOrder"=dword:00000000
"MessageSize"=dword:0000fff7
"szProtocol"=hex(2):40,00,25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,\
  00,6f,00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,\
  5c,00,6d,00,73,00,77,00,73,00,6f,00,63,00,6b,00,2e,00,64,00,6c,00,6c,00,2c,\
  00,2d,00,36,00,30,00,31,00,30,00,31,00
"ProviderFlags"=dword:00000008
"ServiceFlags"=dword:00020609

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Winsock\2]
"Version"=dword:00000002
"AddressFamily"=dword:00000002
"MaxSockAddrLength"=dword:00000010
"MinSockAddrLength"=dword:00000010
"SocketType"=dword:00000003
"Protocol"=dword:00000000
"ProtocolMaxOffset"=dword:000000ff
"ByteOrder"=dword:00000000
"MessageSize"=dword:00008000
"szProtocol"=hex(2):40,00,25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,\
  00,6f,00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,\
  5c,00,6d,00,73,00,77,00,73,00,6f,00,63,00,6b,00,2e,00,64,00,6c,00,6c,00,2c,\
  00,2d,00,36,00,30,00,31,00,30,00,32,00
"ProviderFlags"=dword:0000000c
"ServiceFlags"=dword:00020609

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Performance]
"Close"="CloseTcpIpPerformanceData"
"Collect"="CollectTcpIpPerformanceData"
"Library"=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,6f,00,\
  74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,50,\
  00,65,00,72,00,66,00,63,00,74,00,72,00,73,00,2e,00,64,00,6c,00,6c,00,00,00
"Object List"="502 510 546 548 582 638 658 1530 1532 1534 1820"
"Open"="OpenTcpIpPerformanceData"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Security]

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider]
"Class"=dword:00000008
"DnsPriority"=dword:000007d0
"HostsPriority"=dword:000001f4
"LocalPriority"=dword:000001f3
"Name"="TCP/IP"
"NetbtPriority"=dword:000007d1
"ProviderPath"=hex(2):25,00,53,00,79,00,73,00,74,00,65,00,6d,00,52,00,6f,00,6f,\
  00,74,00,25,00,5c,00,53,00,79,00,73,00,74,00,65,00,6d,00,33,00,32,00,5c,00,\
  77,00,73,00,6f,00,63,00,6b,00,33,00,32,00,2e,00,64,00,6c,00,6c,00,00,00

